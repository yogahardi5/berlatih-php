<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Belajar Git</title>
</head>
<body>
    <h1>Berlatih Git</h1>

    <?php 
        echo "<h3>Soal No 2 Ubah Huruf </h3>";
        /* 
        2.  Ubah Huruf
            buatlah sebuah file dengan nama ubah-huruf.php. Di dalam file tersebut buatlah sebuah function dengan nama ubah_huruf yang menerima parameter berupa string. function akan mereturn string yang berisi karakter-karakter yang sudah diubah dengan karakter setelahnya dalam susunan abjad “abdcde …. xyz”. Contohnya karakter “a” akan diubah menjadi “b” karakter “x” akan berubah menjadi “y”, dst. 
        */

       function ubah_huruf($string)
        {
           $kata1 = '';
           for ($i=0; $i <strlen($string) ; $i++) 
               {    
                    $kata_a = substr($string, $i,1);
                    $kata_b = ord($kata_a);
                    $kata_c = $kata_b+1;
                    $kata_d = chr($kata_c);
                    $kata1 .= $kata_d;
               }
               
               return $kata1;
        }

        // TEST CASES
        echo ubah_huruf('wow'); // xpx
        echo "<br>";
        echo ubah_huruf('developer'); // efwfmpqfs
        echo "<br>";
        echo ubah_huruf('laravel'); // mbsbwfm
        echo "<br>";
        echo ubah_huruf('keren'); // lfsfo
        echo "<br>";
        echo ubah_huruf('semangat'); // tfnbohbu
?>

</body>
</html>