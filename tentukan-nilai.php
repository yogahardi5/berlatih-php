<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Belajar Git</title>
</head>
<body>
    <h1>Berlatih Git</h1>

    <?php 
        echo "<h3>Soal No 1 Tentukan Nilai</h3>";
        /* 
            1.  Tentukan Nilai
                buatlah sebuah file dengan nama tentukan-nilai.php. Di dalam file tersebut buatlah function dengan nama tentukan_nilai yang menerima parameter berupa integer. dengan ketentuan jika paramater integer lebih besar dari sama dengan 85 dan lebih kecil sama dengan 100 maka akan mereturn String “Sangat Baik” Selain itu jika parameter integer lebih besar sama dengan 70 dan lebih kecil dari 85 maka akan mereturn string “Baik” selain itu jika parameter number lebih besar sama dengan 60 dan lebih kecil dari 70 maka akan mereturn string “Cukup” selain itu maka akan mereturn string “Kurang” 

        */
        // Lakukan Looping Di Sini
      
        function tentukan_nilai($number)
        {
            if ($number>=98&&$number<=100){
                echo "Angka $number, Termasuk Angka yang Sangat Baik <br>";
            }
            elseif ($number>=76&&$number<98) {
                echo "Angka $number, Termasuk Angka yang Baik  <br>";
            }
            elseif ($number>=67&&$number<76) {
                echo "Angka $number, Termasuk Angka yang Cukup  <br>";
            }
            elseif ($number>=43&&$number<67) {
                echo "Angka $number, Termasuk Angka yang Kurang  <br>";
            }
            else{
                echo "Angka yang anda masukan diluar kriteria yang ada";
            }
        }

        //TEST CASES
        echo tentukan_nilai(98); //Sangat Baik
        echo tentukan_nilai(76); //Baik
        echo tentukan_nilai(67); //Cukup
        echo tentukan_nilai(43); //Kurang

?>

</body>
</html>