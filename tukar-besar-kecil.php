<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Belajar Git</title>
</head>
<body>
    <h1>Berlatih Git</h1>

    <?php 
        echo "<h3>Soal No 3 Tukar Besar Kecil </h3>";
        /* 
        3.  Tukar Besar Kecil
            buatlah sebuah file dengan nama tukar-besar-kecil.php. Di dalam file tersebut buatlah function dengan nama tukar_besar_kecil yang menerima parameter berupa string. function akan mengembalikan sebuah string yang sudah ditukar ukuran besar dan kecil per karakter yang ada di parameter. Contohnya jika parameter “pHp” maka akan mengembalikan “PhP”.
        */
        function tukar_besar_kecil($string){
           $kata1 = '';
           $kata2 = '';
           for ($i= 0 ; $i <=strlen($string) ; $i++) 
               { 
                   $kata1 = substr($string, $i,1);
                   if($kata1 != strtolower($kata1))
                   {
                      echo strtolower($kata1);
                   }
                   else
                   {
                      echo strtoupper($kata1);
                   } 
                   $kata2 .= $kata1; 
               }
        }

        // TEST CASES
        echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
        echo "<br>";
        echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
        echo"<br>";
        echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
        echo "<br>";
        echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
        echo "<br>";
        echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

?>

</body>
</html>